<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
class Search
{

    #[Assert\NotBlank]
    private string $searchQuery;


    /**
     * @param array $tickets The list of tickets from the database
     * @param int $threshold The minimum levenshtein distance for a match
     * @return array
     */
    public function search(array $tickets, int $threshold = 3): array
    {
        $result = [];

        foreach($tickets as $ticket) {
            $distance = levenshtein($this->searchQuery, $ticket->getTitle());

            if($distance <= $threshold) {
                $result[] = [$ticket, $distance];
            }
        }

        usort($result, function ($a, $b){
            return $a[1] <=> $b[1];
        });

        return $result;
    }

}