<?php

namespace App\Twig\Components;

use App\Entity\Ticket;
use Symfony\Component\Form\FormInterface;
use Symfony\UX\LiveComponent\ComponentWithFormTrait;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;

#[AsTwigComponent]
class TicketDetails
{
    public Ticket $ticket;


}