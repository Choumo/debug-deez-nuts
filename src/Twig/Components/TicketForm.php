<?php

namespace App\Twig\Components;

use App\Entity\Ticket;
use App\Form\TicketType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\Attribute\LiveProp;
use Symfony\UX\LiveComponent\ComponentWithFormTrait;
use Symfony\UX\LiveComponent\DefaultActionTrait;

#[AsLiveComponent]
class TicketForm extends AbstractController
{
    use ComponentWithFormTrait;
    use DefaultActionTrait;

    #[LiveProp]
    public bool $isFormValid = false;

    #[LiveProp]
    public ?Ticket $initialFormData = null;

    protected function instantiateForm(): FormInterface
    {
        return $this->createForm(TicketType::class, $this->initialFormData);
    }

    public function hasValidationErrors(): bool
    {
        $this->isFormValid = $this->getForm()->isSubmitted() && !$this->getForm()->isValid();

        return $this->isFormValid;
    }

    #[LiveAction]
    public function create(EntityManagerInterface $entityManager)
    {
        try {
            $this->submitForm();

            $ticket = $this->getForm()->getData();

            $entityManager->persist($ticket);
            $entityManager->flush();

            $this->addFlash('success', 'Your ticket has been created, you can now log in');

            return $this->redirectToRoute('ticket_index');
        }catch (\Exception|\Throwable $exception) {
            $this->addFlash('error', $exception->getMessage());
        }
    }
}