<?php

namespace App\Twig\Components\Search;

use App\Entity\Ticket;
use App\Form\SearchType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\UX\LiveComponent\Attribute\AsLiveComponent;
use Symfony\UX\LiveComponent\Attribute\LiveAction;
use Symfony\UX\LiveComponent\ComponentWithFormTrait;
use Symfony\UX\LiveComponent\DefaultActionTrait;
use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;
use Meilisearch\Bundle\SearchService;

#[AsLiveComponent]
class SearchForm extends AbstractController
{
    use ComponentWithFormTrait;
    use DefaultActionTrait;

    protected function instantiateForm(): FormInterface
    {
        return $this->createForm(
            type: SearchType::class
        );
    }

    #[LiveAction]
    public function showSearchResult(SearchService $searchService, EntityManagerInterface $entityManager) {
        try {
            $this->submitForm();

            $hits = $searchService->search($entityManager, Ticket::class, $this->getForm()->get('search')->getData());
            dd($hits);


        } catch (\Exception|\Throwable $exception) {

        }
    }

}