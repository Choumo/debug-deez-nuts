<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\UX\Chartjs\Builder\ChartBuilderInterface;
use Symfony\UX\Chartjs\Model\Chart;

#[Route('/user')]
class UserController extends AbstractController
{

    #[Route('/', name: 'app_user_profile', methods: ["GET"])]
    public function profile(EntityManagerInterface $entityManager, ChartBuilderInterface $chartBuilder): Response
    {

        $chart = $chartBuilder->createChart(
            type: Chart::TYPE_LINE
        );

        $userTickets = $this->getUser()->getTicket();
        $userComments = $this->getUser()->getComments();

        $ticketDates = [];
        foreach ($userTickets as $currentTicket) {
            $ticketDates[] = $currentTicket->getCreatedAt()->format('Y-m-d');
        }


        $chart->setData([
            'labels' => ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            'datasets' => [
                [
                    'label' => 'Comments',
                    'backgroundColor' => 'rgb(255, 99, 132)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => [0, 10, 5, 2, 20, 30, 45],
                ],
                [
                    'label' => 'Tickets',
                    'backgroundColor' => 'rgb(255, 99, 132)',
                    'borderColor' => 'rgb(255, 99, 132)',
                    'data' => [0, 10, 5, 2, 40, 30, 45],
                ],
            ],
        ]);

        $chart->setOptions([
            'scales' => [
                'y' => [
                    'suggestedMin' => 0,
                    'suggestedMax' => 100,
                ],
            ],
        ]);

        return $this->render(
            view: 'user/index.html.twig',
            parameters: [
                'chart' => $chart
            ]
        );
    }

}