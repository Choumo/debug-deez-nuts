<?php

namespace App\Controller;

use App\Entity\Comment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/comment')]
class CommentController extends AbstractController
{
    #[Route('/comment', name: 'app_comment')]
    public function index(): Response
    {
        return $this->render('comment/index.html.twig', [
            'controller_name' => 'CommentController',
        ]);
    }
    
    #[Route('/', name: 'app_create_comment', methods: ["POST"])]
    public function create(Request $request): JsonResponse
    {

    }

    public function update(): JsonResponse
    {

    }

    #[Route('/delete/{id}', name: 'app_comment_delete', methods: ['GET'])]
    public function delete(#[\SensitiveParameter] int $id, EntityManagerInterface $entityManager, Request $request): \Symfony\Component\HttpFoundation\RedirectResponse
    {
        try {
            $commentRepository = $entityManager->getRepository(Comment::class);
            $comment = $commentRepository->find($id);

            $entityManager->remove($comment);
            $entityManager->flush();

            $this->addFlash('success', 'Your comment has successfully deleted !');
        } catch (\Exception|\Throwable $exception) {
            $this->addFlash('error', 'Something went wrong, your comment has not been deleted.');
        } finally {
            return $this->redirect(
                $request->headers->get('referer')
            );
        }

    }
}
