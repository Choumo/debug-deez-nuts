<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Ticket;
use App\Form\CommentType;
use App\Form\TicketType;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Meilisearch\Bundle\SearchService;
use SensitiveParameter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/ticket')]
class TicketController extends AbstractController {
    
    #[Route('/', name: 'ticket_index', methods: ["GET"])]
    public function index(EntityManagerInterface $em): Response 
    {
        $ticket_repository = $em->getRepository(Ticket::class);
        $tickets = $ticket_repository->findAll();

        return $this->render('ticket/index.html.twig', [
            'tickets' => $tickets
        ]); 
    }

    #[Route('/show/{id}', name: 'ticket_read', methods: ["GET", "POST"])]
    public function read(#[SensitiveParameter] int $id, EntityManagerInterface $em, Request $request): Response
    {
        
        $ticket_repository = $em->getRepository(Ticket::class);
        $ticket = $ticket_repository->find($id);

        $form_comment = $this->createForm(CommentType::class);
        $form_comment->handleRequest($request);

        if($form_comment->isSubmitted() && $form_comment->isValid()) {
            $comment = new Comment();
            $comment->setContent($form_comment->get('content')->getData());
            $comment->setAuthor($this->getUser());
            $comment->setTicket($ticket);
            $comment->setCreatedAt(new DateTimeImmutable('now'));

            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('ticket_read', ['id' => $id]);
        }

        $comment_repository = $em->getRepository(Comment::class);
        $comments = $comment_repository->findBy(['ticket' => $ticket]);

        return $this->render('ticket/read.html.twig', [
            'ticket' => $ticket,
            'form_comment' => $form_comment,
            'comments' => $comments
        ]); 
    }

    #[Route('/create', name: 'ticket_create', methods: ["GET", "POST"])]
    public function create(EntityManagerInterface $em, Request $request): Response
    {
        $ticket = new Ticket();
        return $this->render('ticket/create.html.twig', [
            'ticket' => $ticket,
        ]);
    }

    #[Route('/update/{id}', name: 'ticket_update', methods: ["GET"])]
    public function update(#[SensitiveParameter] int $id, EntityManagerInterface $em): Response
    {
        $ticket_repository = $em->getRepository(Ticket::class);
        $ticket = $ticket_repository->find($id);

        return $this->render(
            view: 'ticket/update.html.twig',
            parameters: [
                'ticket' => $ticket
        ]);
    }

    #[Route('/delete/{id}', name: 'app_ticket_delete')]
    public function delete(#[SensitiveParameter] int $id, EntityManagerInterface $entityManager): Response
    {
        try {
            $ticketRepository = $entityManager->getRepository(Ticket::class);
            $ticket = $ticketRepository->find($id);

            $entityManager->remove($ticket);
            $entityManager->flush();

            $this->addFlash(
                type: 'success',
                message: 'your ticket has been successfully deleted'
            );
        } catch (\Exception|\Throwable $exception) {
            $this->addFlash(
                type: 'error',
                message: 'Something went wrong, we could not delete your ticket.'
            );
        } finally {
            return $this->redirectToRoute(
                route: 'ticket_index'
            );
        }
    }

    #[Route('/resolve/{id}', name: 'ticket_resolve')]
    public function resolve(#[SensitiveParameter] int $id, EntityManagerInterface $entityManager): Response
    {
        try {
            $ticketRepository = $entityManager->getRepository(Ticket::class);
            $ticket = $ticketRepository->find($id);

            $ticket->setIsDone(true);
            $entityManager->persist($ticket);
            $entityManager->flush();

            $this->addFlash(
                type: 'success',
                message: 'This ticket has been marked as resolved'
            );
        } catch (\Exception|\Throwable $exception) {
            $this->addFlash(
                type:'error',
                message: "You can't mark this ticket as resolved"
            );
        } finally {
            return $this->redirectToRoute('ticket_read', [
                'id' => $id
            ]);
        }
    }

    #[Route('/search', name: 'ticket_search')]
    public function search(SearchService $searchService, EntityManagerInterface $entityManager): Response
    {
        $hits = $searchService->search($entityManager, Ticket::class, "Lorem");
        dd($hits);
    }
}
