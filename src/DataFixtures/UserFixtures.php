<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{

    public const USER = 'regular-user';

    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail(email: 'account@mail.fr');
        $user->setPassword(password: 'SomeP4ssWord$');

        $manager->persist($user);
        $manager->flush();
    }
}