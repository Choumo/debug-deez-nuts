<?php

namespace App\Listener;

use App\Entity\Ticket;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Symfony\Bundle\SecurityBundle\Security;

#[AsEntityListener(
    event: Events::prePersist,
    method: 'prePersist',
    entity: Ticket::class
)]
#[AsEntityListener(
    event: Events::preUpdate,
    method: 'preUpdate',
    entity: Ticket::class
)]
class TicketListener
{

    private Security $security;

    public function __construct(Security $security) {
        $this->security = $security;
    }

    public function prePersist(Ticket $ticket, PrePersistEventArgs $eventArgs): void
    {
        $ticket->setCreatedAt(
            new \DateTimeImmutable('now')
        );

        $ticket->setAuthor(
            $this->security->getUser()
        );
    }

    public function preUpdate(Ticket $ticket, PreUpdateEventArgs $eventArgs): void
    {
        $ticket->setUpdatedAt(
            new \DateTimeImmutable('now')
        );
    }

}