<?php

namespace App\Listener;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

#[AsEntityListener(
    event: Events::prePersist,
    method: 'prePersist',
    entity: User::class)
]
class UserListener
{
    private UserPasswordHasherInterface $userPasswordHasher;

    public function __construct(UserPasswordHasherInterface $userPasswordHasher) {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    /**
     * This function will automatically hash the password of a new user
     * and set his createdAt attribute with the current DateTime
     * @param User $user
     * @param PrePersistEventArgs $event
     * @return void
     */
    public function prePersist(User $user, PrePersistEventArgs $event): void
    {
        $user->setPassword(
            $this->encodePassword(
                $user,
                $user->getPassword())
        );

        $user->setCreatedAt(
          new \DateTimeImmutable('now')
        );
    }

    private function encodePassword(User $user, string $password): string
    {
        return $this->userPasswordHasher->hashPassword($user, $password);
    }
}